class Input {
  constructor(
    years = 0,
    fok = 0,
    apy = 0,
    ira = 0,
    hsa = 0,
    aft = 0,
    wyears = 0,
    wapy = 0,
    payout = 0
  ) {
    this._years = years;
    this._fok = parseFloat(fok);
    this._apy = parseFloat(apy);
    this._ira = parseFloat(ira);
    this._hsa = parseFloat(hsa);
    this._aft = parseFloat(aft);
    this._data = null;
    this._interest = 0;

    this._wyears = wyears;
    this._payout = parseFloat(payout);
    this._wapy = parseFloat(wapy);
  }

  get data() {
    return this._data;
  }

  fv(input, rate) {
    const total = input * (1 + rate);
    const interest = total - input;
    return { total: total, interest: interest };
  }

  compute_stats() {
    const years = [];
    const average_gain_moving = [];
    const interest = 0;
    let total = 0;

    // Calculate core statistics
    for (let year = 1; year <= this._years; year++) {
      total += this._fok;
      total += this._hsa;
      total += this._ira;
      total += this._aft;

      if (year !== this.years) {
        let ret = this.fv(total, this._apy);
        total = ret.total;
        this._interest += ret.interest;
      }

      years.push(year);
      average_gain_moving.push(total);
    }

    let winterest = 0;
    let wtotal = total;
    for (let year = 1; year <= this._wyears; year++) {
      wtotal -= this._payout;
      if (year !== this.years) {
        let ret = this.fv(wtotal, this._wapy);
        wtotal = ret.total;
        winterest += ret.interest;
      }
    }

    // Create chart attribute
    this._data = {
      labels: years,
      series: [average_gain_moving],
    };
    // Average gain based on chance and amount paid
    return {
      principle: total - this._interest,
      interest: this._interest,
      total: total,
      wtotal: wtotal,
      winterest,
      winterest,
    };
  }
}

class Results {
  constructor(
    principle = 0,
    interest = 0,
    total = 0,
    payout = 0,
    left = 0,
    winterest = 0
  ) {
    this._principle = parseInt(principle);
    this._interest = parseInt(interest);
    this._total = parseInt(total);
    this._withdraw_payout = parseInt(payout);
    this._withdraw_left = parseInt(left);
    this._withdraw_interest = parseInt(winterest);
  }

  calculateWithdraw() {
    return;
  }

  updateOutput() {
    const principle = document.getElementById("principle");
    principle.innerHTML = `$${this._principle.toLocaleString()}`;

    const interest = document.getElementById("interest");
    interest.innerHTML = `$${this._interest.toLocaleString()}`;

    const total = document.getElementById("total");
    total.innerHTML = `$${this._total.toLocaleString()}`;

    //

    const withdraw_total = document.getElementById("withdraw_total");
    withdraw_total.innerHTML = `$${this._total.toLocaleString()}`;

    const withdraw_payout = document.getElementById("withdraw_payout_copy");
    withdraw_payout.innerHTML = `$${this._withdraw_payout.toLocaleString()}`;

    const withdraw_interest = document.getElementById("withdraw_interest");
    withdraw_interest.innerHTML = `$${this._withdraw_interest.toLocaleString()}`;

    const withdraw_left = document.getElementById("withdraw_left");
    withdraw_left.innerHTML = `$${this._withdraw_left.toLocaleString()}`;
  }
}

function create_chart(data) {
  new Chartist.Line(".ct-chart", data, {
    showArea: true,
    chartPadding: {
      left: 20,
      bottom: 20,
    },
    plugins: [
      Chartist.plugins.ctThreshold({
        threshold: 4,
      }),
    ],
  });
}

function go() {
  const years = document.getElementById("years_input").value;
  const fok = document.getElementById("fok_input").value;
  const apy = document.getElementById("apy_input").value;
  const ira = document.getElementById("ira_input").value;
  const hsa = document.getElementById("hsa_input").value;
  const aft = document.getElementById("aft_input").value;
  const withdraw_years = document.getElementById("withdraw_years_input").value;
  const withdraw_payout = document.getElementById("withdraw_payout").value;
  const withdraw_apy = document.getElementById("withdraw_apy_input").value;

  const i = new Input(
    years,
    fok,
    apy,
    ira,
    hsa,
    aft,
    withdraw_years,
    withdraw_apy,
    withdraw_payout
  );
  const ret = i.compute_stats();
  let results = new Results(
    ret.principle,
    ret.interest,
    ret.total,
    withdraw_payout,
    ret.wtotal,
    ret.winterest
  );
  results.calculateWithdraw();
  results.updateOutput();
  create_chart(i.data);
}

const outputs = document.querySelectorAll(".output");
outputs.forEach(function (elem) {
  // Just so we have a blank chart
  create_chart();

  elem.addEventListener("input", function () {
    corresponding_element = elem.id.split("_")[0];
    let slider = document.getElementById(corresponding_element);
    slider.value = elem.value;
    go();
  });
});

let sliders = document.querySelectorAll(".slider");

sliders.forEach(function (elem) {
  elem.addEventListener("input", function () {
    let box = document.getElementById(`${elem.id}_input`);
    box.value = elem.value;
    go();
  });
});
